To use this iOS project, you must do the following:

1. sign up a facebook account
2. go to developer.facebook.com and register as developer
3. create a facebook app
4. add iOS platform from settings on left menu in app page
5. add your app bundle to under bundle id. You can have multiple id so you may want to add production bundle id as well.
6. copy the app id and update info.plist, custom url and loginViewController FACEBOOK_APP_ID