//
//  GFSFacebookManager.h
//  GT_FB_Sample
//
//  Created by chris on 22/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FacebookClient/FacebookClient.h"

@interface GFSFacebookManager : NSObject

+ (void) statusShareWithMessage:(NSString *) message completion:(FBRequestHandler) completion;

+ (void) userInfoWithCompletion:(void (^)(id response, NSError* error))completion;

+ (void) systemStatusShareWithMessage:(NSString *) message completion:(void (^)(id response, NSError* error))completion;

@end
