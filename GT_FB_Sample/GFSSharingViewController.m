//
//  GFSSharingViewController.m
//  GT_FB_Sample
//
//  Created by chris on 22/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "GFSSharingViewController.h"
#import "GFSStatusSharingViewController.h"
#import <Social/Social.h>
#import <FacebookSDK/FacebookSDK.h>

typedef NS_ENUM(NSInteger, GFSSharingActionSheetIndex) {
    GFSSharingActionSheetIndexLink,
    GFSSharingActionSheetIndexPhoto,
    GFSSharingActionSheetIndexCancel
};

@interface GFSSharingViewController() <UIActionSheetDelegate>

@property (nonatomic, strong) NSDictionary *fbShareParamsDict;
@property (nonatomic, strong) FBLinkShareParams *linkShareParams;
@property (nonatomic) GFSStatusSharingType shareType;

@end

@implementation GFSSharingViewController

#pragma mark - Life Cycle
- (void) viewDidLoad {
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:GFSStoryboardStatusShareSegueIdentifier]) {
        
        if ([segue.destinationViewController isKindOfClass:[GFSStatusSharingViewController class]]) {
            GFSStatusSharingViewController *statusShareViewController = (GFSStatusSharingViewController *) segue.destinationViewController;
            statusShareViewController.shareType = self.shareType;
        }
    }
}

#pragma mark - Getter 

- (NSDictionary *) fbShareParamsDict {
    if (!_fbShareParamsDict) {
        _fbShareParamsDict = @{@"name": @"Apple Watch",
                           @"caption": @"Hello Apple Watch",
                           @"description": @"The first version of Apple Watch",
                           @"link":@"http://www.apple.com/watch",
                           @"picture":@"http://cdn.redmondpie.com/wp-content/uploads/2014/09/Apple-Watch-logo-main1.png"};
    }
    return _fbShareParamsDict;
}

- (FBLinkShareParams *) linkShareParams {
    if (!_linkShareParams) {
        _linkShareParams = [[FBLinkShareParams alloc] initWithLink:[NSURL URLWithString:self.fbShareParamsDict[@"link"]]
                                                              name:self.fbShareParamsDict[@"name"]
                                                           caption:self.fbShareParamsDict[@"caption"]
                                                       description:self.fbShareParamsDict[@"description"]
                                                           picture:[NSURL URLWithString:@"picture"]];
    }
    return _linkShareParams;
}

#pragma mark - Actions
- (IBAction)facebookAppSharingButtonDidTap:(UIButton *)sender {
    
    // If the Facebook app is installed and we can present the share dialog
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"What do want to share?"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Link", @"Photo", nil];
    [actionSheet showInView:self.view];
}

- (IBAction)facebookGraphAPISharingButtonDidTap:(UIButton *)sender
{
    self.shareType = GFSStatusSharingTypeFacebookSDK;
    [self performSegueWithIdentifier:GFSStoryboardStatusShareSegueIdentifier sender:sender];
}

- (IBAction)webDialogSharingButtonDidTap:(UIButton *)sender
{
    // Put together the dialog parameters
    
    // Show the feed dialog
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:self.fbShareParamsDict
                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                  if (error) {
                                                      // An error occurred, we need to handle the error
                                                      // See: https://developers.facebook.com/docs/ios/errors
                                                      NSLog(@"Error publishing story: %@", error.description);
                                                  } else {
                                                      if (result == FBWebDialogResultDialogNotCompleted) {
                                                          // User cancelled.
                                                          NSLog(@"User cancelled.");
                                                      } else {
                                                          // Handle the publish feed callback
//                                                          NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                          
//                                                          if (![urlParams valueForKey:@"post_id"]) {
//                                                              // User cancelled.
//                                                              NSLog(@"User cancelled.");
//                                                              
//                                                          } else {
//                                                              // User clicked the Share button
//                                                              NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
//                                                              NSLog(@"result %@", result);
//                                                          }
                                                      }
                                                  }
                                              }];
}

- (IBAction)systemSharingButtonDidTap:(UIButton *)sender
{
    //This uses the system provided sharing method and it will show as sharing via iOS
    //if you want to share with your facebook app
    //you have to make your own UI and share via Graph API
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [composeViewController setInitialText:@"Hello Watch!"];
        [composeViewController addImage:[UIImage imageNamed:@"watch"]];
        [composeViewController addURL:[NSURL URLWithString:@"http://www.apple.com/watch"]];
        __weak typeof(self)weakSelf = self;
        composeViewController.completionHandler = ^(SLComposeViewControllerResult result) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf) {
                return;
            }
            [strongSelf handleSLComposeViewControllerCompletion:result];
        };
        [self presentViewController:composeViewController animated:YES completion:NULL];        
        
    } else {
        NSLog(@"SLComposeViewController is unavailable for Facebook");
    }
}

- (IBAction)systemGraphApiSharingButtonDidTap:(UIButton *)sender {
    self.shareType = GFSStatusSharingTypeSystem;
    [self performSegueWithIdentifier:GFSStoryboardStatusShareSegueIdentifier sender:sender];
}
- (IBAction)activityControllerButtonDidTap:(UIButton *)sender {
    
    UIImage *watchImage = [UIImage imageNamed:@"watch"];
    NSString *caption = @"Hello Watch!";
    NSURL *watchUrl = [NSURL URLWithString:@"http://www.apple.com/watch"];
    
    //UIActivityViewController can not directly access the image.xcassets
    //We have to copy the image to the cache directory
    //and retrieve it from the cache directory
    NSData *imageData = UIImagePNGRepresentation(watchImage);
    NSString *tempFilePath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"watch.png"];
    [imageData writeToFile:tempFilePath atomically:YES];
    NSURL *fileUrl = [NSURL fileURLWithPath:tempFilePath];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[fileUrl, caption,watchUrl]
                                                                                         applicationActivities:nil];
    [self presentViewController:activityViewController animated:YES completion:^{
        
    }];

}

#pragma mark - Private

- (void) handleSLComposeViewControllerCompletion:(SLComposeViewControllerResult) result {
    switch (result) {
        case SLComposeViewControllerResultCancelled:
            NSLog(@"user cancel sharing");
            break;
        case SLComposeViewControllerResultDone:
            NSLog(@"user have done sharing");
            break;
    }
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case GFSSharingActionSheetIndexLink:
        {
            if ([FBDialogs canPresentShareDialogWithParams:self.linkShareParams]) {
                // Present the share dialog
                // This will call the native facebook
                [FBDialogs presentShareDialogWithParams:self.linkShareParams clientState:nil handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {

                }];
            } else {
                // Present the feed dialog
                [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                                       parameters:self.fbShareParamsDict
                                                          handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                              if (error) {
                                                                  // An error occurred, we need to handle the error
                                                                  // See: https://developers.facebook.com/docs/ios/errors
                                                                  NSLog(@"Error publishing story: %@", error.description);
                                                              } else {
                                                                  if (result == FBWebDialogResultDialogNotCompleted) {
                                                                      // User cancelled.
                                                                      NSLog(@"User cancelled.");
                                                                  } else {
                                                                  }
                                                              }
                                                          }
                 ];
                
            }
            break;
        }
        case GFSSharingActionSheetIndexPhoto:
        {
            UIImage *image = [UIImage imageNamed:@"watch"];
            FBPhotoParams *photoParams = [[FBPhotoParams alloc] initWithPhotos:@[image, image, image]];
            
            if ([FBDialogs canPresentShareDialogWithPhotos]) {
                [FBDialogs presentShareDialogWithPhotoParams:photoParams clientState:nil handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                    if (!error) {
                        NSLog(@"results = %@" , results);
                    }
                }];
            } else {
                
            }
            
            break;
        }
        case GFSSharingActionSheetIndexCancel:
            break;
    }
}



@end
