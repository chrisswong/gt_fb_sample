//
//  GFSStatusSharingViewController.h
//  GT_FB_Sample
//
//  Created by chris on 22/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GFSBaseViewController.h"

typedef NS_ENUM(NSInteger, GFSStatusSharingType) {
    GFSStatusSharingTypeFacebookSDK,
    GFSStatusSharingTypeSystem
};

@interface GFSStatusSharingViewController : GFSBaseViewController

@property (nonatomic) GFSStatusSharingType shareType;

@end
