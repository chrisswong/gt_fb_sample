//
//  FacebookClient.m
//  ShareEngine
//
//  Created by Kevin Chan on 15/8/13.
//  Copyright (c) 2013 Green Tomato. All rights reserved.
//

#import "FacebookClient.h"

NSString *FacebookDidLogin = @"FacebookDidLogin";

@interface FacebookClient()

@property (nonatomic, copy) FBRequestBlock completionHandler;
@property (nonatomic, strong) FBRequestConnection *fbRequestConnection;

@end

@implementation FacebookClient

+ (id)sharedClient
{
    static FacebookClient *__sharedClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        if (__sharedClient == nil) {
            __sharedClient = [[FacebookClient alloc] init];
        }
        
    });
    
    return __sharedClient;
}

#pragma mark -

// override to provide your custom set of initial read permissions.
+ (NSArray *)readPermissions
{
    return @[@"email"];
//    return @[@"email", @"user_birthday"];
}

// override to provide your custom set of write permissions.
+ (NSArray *)writePermissions
{
    return @[@"publish_actions"];
}

+ (NSDictionary *)translateError:(NSError *)error
{
    // Handle authentication errors
    
    NSString *alertMessage, *alertTitle;
    
    if (error.fberrorShouldNotifyUser) {
        // If the SDK has a message for the user, surface it. This conveniently
        // handles cases like password change or iOS6 app slider state.
        alertTitle = @"Something Went Wrong";
        alertMessage = error.fberrorUserMessage;
    } else if (error.fberrorCategory == FBErrorCategoryAuthenticationReopenSession) {
        // It is important to handle session closures as mentioned. You can inspect
        // the error for more context but this sample generically notifies the user.
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
    } else if (error.fberrorCategory == FBErrorCategoryUserCancelled) {
        // The user has cancelled a login. You can inspect the error
        // for more context. For this sample, we will simply ignore it.
        NSLog(@"user cancelled login");
    } else {
        // For simplicity, this sample treats other errors blindly, but you should
        // refer to https://developers.facebook.com/docs/technical-guides/iossdk/errors/ for more information.
        alertTitle  = @"Unknown Error";
        alertMessage = @"Error. Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertTitle && alertMessage) {
        return @{@"title":alertTitle, @"message":alertMessage};
    }
    
    return nil;
}


- (id)init
{
    self = [super init];
    if (self)
    {
        FBSession *session = [FBSession activeSession];
        
        if (session.state == FBSessionStateCreatedTokenLoaded) {
            [session openWithCompletionHandler:nil];
        }
        
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note){
            [[FBSession activeSession] handleDidBecomeActive];
        }];
        
    }
    
    return self;
}

- (void)login:(FBRequestBlock)completionHandler
{
    
    if ([[FBSession activeSession] isOpen]) {
        //If the session is already open , tell outside has logged in
        completionHandler(YES, nil, nil);
    } else {
        
        [FBSession openActiveSessionWithReadPermissions:[[self class] readPermissions]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState status, NSError *error) {
             
             switch (status) {
                 case FBSessionStateOpen: {
                     
                     completionHandler(YES, nil, nil);
                     [[NSNotificationCenter defaultCenter] postNotificationName:FacebookDidLogin object:nil userInfo:nil];
                     break;
                 }
                 case FBSessionStateClosedLoginFailed:
                 case FBSessionStateClosed: {
                     // Close active session
                     [[FBSession activeSession] closeAndClearTokenInformation];
                     completionHandler(NO, nil, error);
                     
                     break;
                 }
                 default:
                     
                     completionHandler(NO, nil, error);
                     break;
             }
             
             if (error) {
                NSLog( @"%@", [[self class] translateError:error]);
             }
         }];
        
        
    }
}

- (void) loginInPublishMode:(FBRequestBlock)completionHandler {
    if ([[FBSession activeSession] isOpen]) {
        //If the session is already open , tell outside has logged in
        completionHandler(YES, nil, nil);
    } else {
        
        [FBSession openActiveSessionWithPublishPermissions:[[self class] writePermissions]
                                           defaultAudience:FBSessionDefaultAudienceFriends
                                              allowLoginUI:YES
                                         completionHandler:
         ^(FBSession *session, FBSessionState status, NSError *error) {
             
             switch (status) {
                 case FBSessionStateOpen: {
                     
                     completionHandler(YES, nil, nil);
                     [[NSNotificationCenter defaultCenter] postNotificationName:FacebookDidLogin object:nil userInfo:nil];
                     break;
                 }
                 case FBSessionStateClosedLoginFailed:
                 case FBSessionStateClosed: {
                     // Close active session
                     [[FBSession activeSession] closeAndClearTokenInformation];
                     completionHandler(NO, nil, error);
                     
                     break;
                 }
                 default:
                     
                     completionHandler(NO, nil, error);
                     break;
             }
             
             if (error) {
                 NSLog( @"%@", [[self class] translateError:error]);
             }
         }];
        
        
    }
}

- (void)logout:(FBRequestBlock)completionHandler
{
    [[FBSession activeSession] closeAndClearTokenInformation];
    
    if (completionHandler) {
        completionHandler(YES, nil, nil);        
    }

    
}

- (BOOL)isLoggedIn
{
    return [[FBSession activeSession] isOpen];
}



- (void)showDialogWithParameters:(NSDictionary *)parameters handler:(FBWebDialogHandler)handler
{
   
    [FBWebDialogs presentFeedDialogModallyWithSession:[FBSession activeSession]
                                           parameters:parameters
                                              handler:handler];
}

- (void)requestForGraphPath:(NSString *)path parameters:(NSDictionary *)dictionary completionHandler:(FBRequestHandler)handler
{
    [self requestForGraphPath:path parameters:dictionary HTTPMethod:@"GET" completionHandler:handler];
}

- (void)publishGraphRequest:(NSString *)path parameters:(NSDictionary *)dictionary completionHandler:(FBRequestHandler)handler 
{
    
    if ([self isLoggedIn] == NO)
    {
        NSLog(@"Pre-condition: PLEASE LOGIN!");
        return;
    }
    
    void(^publishAction)(void) = ^{
        [self requestForGraphPath:path parameters:dictionary HTTPMethod:@"POST" completionHandler:handler];
    };
    
    // Grant Publish Permission before post reqeust.
    NSMutableArray *unauthorizedPermissions = [NSMutableArray array];
    for (NSString *permission in [[self class] writePermissions]) {
        
        BOOL noPermission = ([FBSession.activeSession.permissions indexOfObject:permission] == NSNotFound);
        
        if (noPermission) {
            [unauthorizedPermissions addObject:permission];
        }
    }
    
    if ([unauthorizedPermissions count] == 0) {
        publishAction();
    } else {
        [[FBSession activeSession] requestNewPublishPermissions:unauthorizedPermissions
                                                defaultAudience:FBSessionDefaultAudienceFriends
                                              completionHandler:
         ^(FBSession *session, NSError *error) {
             
             if (!error) {
                 publishAction();
             } else {
                 handler(nil, nil, error);
             }
         }];
    } 
    
    
}

- (void)requestForGraphPath:(NSString *)path
                 parameters:(NSDictionary *)dictionary
                 HTTPMethod:(NSString *)httpMethod
          completionHandler:(FBRequestHandler)handler
{
    
    // Create connection object
    FBRequest *request = [[FBRequest alloc] initWithSession:[FBSession activeSession]
                                                  graphPath:path
                                                 parameters:dictionary
                                                 HTTPMethod:httpMethod];
    
    [self.fbRequestConnection cancel];
    
    FBRequestConnection* newConnection = [[FBRequestConnection alloc] initWithTimeout:30];
    self.fbRequestConnection = newConnection;
    
    [newConnection addRequest:request completionHandler:
     ^(FBRequestConnection *connection, id result, NSError *error)
     {
         if (error.fberrorCategory == FBErrorCategoryPermissions) {
             NSLog(@"Permission Issue!");
         } else {
             handler(connection, result, error);
         }
     }];
    
    [newConnection start];
    
    
    
}

#pragma mark - SSO URL Handling

- (BOOL)handleOpenURL:(NSURL *)url
{
    return [[FBSession activeSession] handleOpenURL:url];
}

- (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
{
    
    BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication fallbackHandler:^(FBAppCall *call) {
        
        if (call.accessTokenData) {
            
            if([[FBSession activeSession] isOpen]){
                NSLog(@"INFO: Ignoring app link because current session is open.");
            } else {
                [self handleAppLink:call.accessTokenData];
            }
        }
        
    }];
    
    // add app-specific handling code here
    return wasHandled;
}



#pragma mark - Private

// Helper method to wrap logic for handling app links.
- (void)handleAppLink:(FBAccessTokenData *)appLinkToken {
    // Initialize a new blank session instance...
    FBSession *appLinkSession = [[FBSession alloc] initWithAppID:nil
                                                     permissions:nil
                                                 defaultAudience:FBSessionDefaultAudienceNone
                                                 urlSchemeSuffix:nil
                                              tokenCacheStrategy:[FBSessionTokenCachingStrategy nullCacheInstance] ];
    [FBSession setActiveSession:appLinkSession];
    // ... and open it from the App Link's Token.
    [appLinkSession openFromAccessTokenData:appLinkToken completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        // Forward any errors to the FBLoginView delegate.
        if (error) {
            NSLog(@"Error!");
        }
    }];
}

@end
