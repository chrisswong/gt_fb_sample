//
//  FacebookClient.h
//  ShareEngine
//
//  Created by Kevin Chan on 15/8/13.
//  Copyright (c) 2013 Green Tomato. All rights reserved.
//
//  This client wrap the common use methods.
//  So it is much easy to maniplute facebook SDK.
//  in case this class does not provide the function you need.
//  You can directly use FacebookSDK instead.
//
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

typedef void (^FBRequestBlock)(BOOL success,id result,NSError *error);

extern NSString *FacebookDidLogin;

@interface FacebookClient : NSObject

/**
 *	Singleton Method
 *
 *	@return	FacebookClient object. 
 */
+ (id)sharedClient;

#pragma mark - 

// override to provide your custom set of initial read permissions.
+ (NSArray *)readPermissions;

// override to provide your custom set of write permissions.
+ (NSArray *)writePermissions;

+ (NSDictionary *)translateError:(NSError *)error;


#pragma mark - 

// session management

- (void)login:(FBRequestBlock)completionHandler;
- (void) loginInPublishMode:(FBRequestBlock)completionHandler;
- (void)logout:(FBRequestBlock)completionHandler;

- (BOOL)isLoggedIn;

- (BOOL)handleOpenURL:(NSURL *)url;
- (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication;

// Open Graph Request
- (void)requestForGraphPath:(NSString *)path parameters:(NSDictionary *)dictionary completionHandler:(FBRequestHandler)handler;

/**
 *	Publish request using Graph API
 *  To use this method, make sure you already signIn Facebook.
 *  (i.e. ** -isLoggedIn ** return YES )
 */
- (void)publishGraphRequest:(NSString *)path parameters:(NSDictionary *)dictionary completionHandler:(FBRequestHandler)handler;

/**
 *	Show Dialog 
 *
 *
 *	@param	parameters	parameters for dialog display, all item should be **NSString** object.
 *      Include the following keys 
 *          to      - receiver facebook Id
 *          link    - Page URL you want to share.
 *          picture - Thumbnail URL
 *          name    - 
 *          caption - 
 *          description - Page summary.
 *
 *
 *	@param	handler
 */
- (void)showDialogWithParameters:(NSDictionary *)parameters handler:(FBWebDialogHandler)handler;

@end
