//
//  GFSLoginViewController.m
//  GT_FB_Sample
//
//  Created by chris on 22/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "GFSLoginViewController.h"
#import <Accounts/Accounts.h>

#import "FacebookClient.h"

@interface GFSLoginViewController ()

@property (nonatomic, strong) NSDictionary *systemLoginReadAccessInfo;
@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, weak) FacebookClient *facebookClient;

@end

@implementation GFSLoginViewController

#pragma mark - Life Cycle
- (void) viewDidLoad {
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountDidChange:) name:ACAccountStoreDidChangeNotification object:nil];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ACAccountStoreDidChangeNotification object:nil];
    
}
#pragma mark - Notification 

- (void) accountDidChange:(NSNotification *) notification {
    [self attemptRenewCredentials];
}

#pragma mark - Getters

- (FacebookClient *) facebookClient {
    if (!_facebookClient) {
        _facebookClient = [FacebookClient sharedClient];
    }
    return _facebookClient;
}

- (ACAccountStore *) accountStore {
    if (!_accountStore) {
        _accountStore = [[ACAccountStore alloc] init];
    }
    return _accountStore;
}

- (NSArray *) readPermissions {
    return @[@"email"];
}

- (NSDictionary *) systemLoginReadAccessInfo {
    return @{ ACFacebookAppIdKey : FACEBOOK_APP_ID,
              ACFacebookAudienceKey : ACFacebookAudienceFriends,
              ACFacebookPermissionsKey : [self readPermissions] };
}

#pragma mark - Private

- (void) attemptRenewCredentials {
    
    ACAccountType *facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    ACAccount *grantedFacebookAccount = (ACAccount *) [[self.accountStore accountsWithAccountType:facebookAccountType] lastObject];
    
    if (grantedFacebookAccount) {
        [self.accountStore renewCredentialsForAccount:grantedFacebookAccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {
            
            if (error) {
                NSLog(@"renew error : %@", error.localizedDescription);
            } else {
                
                switch (renewResult) {
                    case ACAccountCredentialRenewResultFailed:
                        NSLog(@"A non-user-initiated cancel of the prompt.");
                        break;
                    case ACAccountCredentialRenewResultRejected:
                        NSLog(@"User declined permission");
                        break;
                    case ACAccountCredentialRenewResultRenewed:
                        NSLog(@"the token is renewed");
                        break;
                }
            }
        }];
    }
}

- (void) alertViewWithMessage:(NSString *) alertMessage {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                        message:alertMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
    [alertView show];
}


#pragma mark - Action

- (IBAction)facebookAppLoginButtonDidTap:(UIButton *)sender
{
    if (!self.facebookClient.isLoggedIn) {
        
        __weak typeof(self)weakSelf = self;
        [self.facebookClient login:^(BOOL success, id result, NSError *error) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            
            if (!strongSelf) {
                return ;
            }
            
            if (error) {
                [self alertViewWithMessage:[NSString stringWithFormat:@"Login error = %@" , error.localizedDescription]];
            } else {
                [self alertViewWithMessage:@"Login success"];
            }
        }];
        
    } else {
        [self alertViewWithMessage:@"You have already login"];
    }
    
}
- (IBAction)webDialogLoginButtonDidTap:(UIButton *)sender
{
    
}
- (IBAction)systemLoginButtonDidTap:(UIButton *)sender
{
    ACAccountType *facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    [self.accountStore requestAccessToAccountsWithType:facebookAccountType options:self.systemLoginReadAccessInfo completion:^(BOOL granted, NSError *error) {
        
        if (granted) {
            
            ACAccount *grantedFacebookAccount = (ACAccount *) [[self.accountStore accountsWithAccountType:facebookAccountType] lastObject];
            if (grantedFacebookAccount) {
                
                NSLog(@"accountDescription = %@" , grantedFacebookAccount.accountDescription);
                NSLog(@"username = %@", grantedFacebookAccount.username );
                NSLog(@"userFullName = %@", grantedFacebookAccount.userFullName);
                NSLog(@"access_token = %@", grantedFacebookAccount.credential.oauthToken);
                
                //You can make use of this access_token to call Graph API
                
            } else {
                NSLog(@"grantedFacebookAccount is nil");
            }
            
        } else {
            NSLog(@"Facebook access is not granted");
            switch (error.code) {
                case ACErrorAccountNotFound:
                    NSLog(@"No facebook account found!");
                    break;
                case ACErrorAccountTypeInvalid:
                    NSLog(@"Invalid facebook account!");
                default:
                    NSLog(@"Facebook access error : %@" , error.localizedDescription);
                    break;
            }
        }
    }];
}

@end
