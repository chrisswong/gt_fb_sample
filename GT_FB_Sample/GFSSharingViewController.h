//
//  GFSSharingViewController.h
//  GT_FB_Sample
//
//  Created by chris on 22/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GFSConstants.h"
#import "GFSBaseViewController.h"

@interface GFSSharingViewController : GFSBaseViewController

@end
