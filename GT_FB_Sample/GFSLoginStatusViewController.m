//
//  GFSLoginStatusViewController.m
//  GT_FB_Sample
//
//  Created by Chris on 24/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "GFSLoginStatusViewController.h"
#import <Accounts/Accounts.h>
#import "GFSConstants.h"
#import "GFSFacebookManager.h"

@interface GFSLoginStatusViewController ()

@property (weak, nonatomic) IBOutlet UILabel *fbSDKLoginStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *systemLoginStatusLabel;
@property (nonatomic, strong) NSDictionary *systemLoginReadAccessInfo;
@property (nonatomic, strong) ACAccountStore *accountStore;

@end

@implementation GFSLoginStatusViewController

#pragma mark - Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateUI];
}

#pragma mark - Getter

- (ACAccountStore *) accountStore {
    if (!_accountStore) {
        _accountStore = [[ACAccountStore alloc] init];
    }
    return _accountStore;
}

- (NSDictionary *) systemLoginReadAccessInfo {
    return @{ ACFacebookAppIdKey : FACEBOOK_APP_ID,
              ACFacebookAudienceKey : ACFacebookAudienceFriends,
              ACFacebookPermissionsKey : @[@"email"] };
}


#pragma mark - Private 

- (void) updateUI {
    [self updateSystemLoginStatus];
    [self updateFBSDKLoginStatus];
}

- (void) updateSystemLoginStatus {
    
    ACAccountType *facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    [self.accountStore requestAccessToAccountsWithType:facebookAccountType options:self.systemLoginReadAccessInfo completion:^(BOOL granted, NSError *error) {
        
        if (granted) {
            
            ACAccount *grantedFacebookAccount = (ACAccount *) [[self.accountStore accountsWithAccountType:facebookAccountType] lastObject];
            if (grantedFacebookAccount) {
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    self.systemLoginStatusLabel.text = [NSString stringWithFormat:@"%@ (%@)", grantedFacebookAccount.userFullName, grantedFacebookAccount.username];
                });

                
            } else {
                NSLog(@"grantedFacebookAccount is nil");
            }
            
        } else {
            NSLog(@"Facebook access is not granted");
            switch (error.code) {
                case ACErrorAccountNotFound:
                    NSLog(@"No facebook account found!");
                    break;
                case ACErrorAccountTypeInvalid:
                    NSLog(@"Invalid facebook account!");
                default:
                    NSLog(@"Facebook access error : %@" , error.localizedDescription);
                    break;
            }
            dispatch_sync(dispatch_get_main_queue(), ^{
                self.systemLoginStatusLabel.text = @"Your app did not grant the access or error occured.";
            });
        }
    }];
}

- (void) updateFBSDKLoginStatus {
    
     self.fbSDKLoginStatusLabel.text = @"Loading...";
    [GFSFacebookManager userInfoWithCompletion:^(id response, NSError *error) {
        if (!error) {
            NSDictionary *fbDict = (NSDictionary *) response;
            self.fbSDKLoginStatusLabel.text = [NSString stringWithFormat:@"%@ (%@)", fbDict[@"name"], fbDict[@"email"]];
        } else {
            self.fbSDKLoginStatusLabel.text = error.localizedDescription;
        }
    }];
}

@end
