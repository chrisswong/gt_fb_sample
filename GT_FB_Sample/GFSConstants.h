//
//  GFSConstants.h
//  GT_FB_Sample
//
//  Created by chris on 22/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#ifndef GT_FB_Sample_GFSConstants_h
#define GT_FB_Sample_GFSConstants_h

#define FACEBOOK_APP_ID @"805461462862771"

#import <Accounts/Accounts.h>

static NSString* GFSStoryboardStatusShareSegueIdentifier = @"Status Share";

#endif
