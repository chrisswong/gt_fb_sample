//
//  GFSFacebookManager.m
//  GT_FB_Sample
//
//  Created by chris on 22/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "GFSFacebookManager.h"
#import "GFSConstants.h"
#import <Social/Social.h>
#define FACEBOOK_CLIENT [FacebookClient sharedClient]

@implementation GFSFacebookManager

+ (void) statusShareWithMessage:(NSString *) message completion:(FBRequestHandler) completion {
    
    [FACEBOOK_CLIENT loginInPublishMode:^(BOOL success, id result, NSError *error) {
        if (!error) {
            NSDictionary *params = @{@"message": message };
            
            [FACEBOOK_CLIENT publishGraphRequest:@"/me/feed" parameters:params completionHandler:completion];
        } else {
            NSLog(@"can not get publish permission");
        }
    }];
}

+ (void) userInfoWithCompletion:(void (^)(id response, NSError* error))completion {
    
    if ([FACEBOOK_CLIENT isLoggedIn]) {
        [FACEBOOK_CLIENT requestForGraphPath:@"/me" parameters:nil completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                completion(result, nil);
            } else {
                completion(nil,error);
            }
        }];
    } else {
        
        NSString *errorDomain = [NSString stringWithFormat:@"%@.error",[[NSBundle mainBundle] bundleIdentifier]];
        NSError *error = [NSError errorWithDomain:errorDomain
                                             code:9999
                                         userInfo:@{NSLocalizedDescriptionKey:@"User did not login"}];
        completion(nil, error);
    }
}

+ (void) systemStatusShareWithMessage:(NSString *) message completion:(void (^)(id response, NSError* error))completion {
    
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *facebookAccountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    
    NSDictionary *dict = @{ ACFacebookAppIdKey : FACEBOOK_APP_ID,
                            ACFacebookAudienceKey : ACFacebookAudienceFriends,
                            ACFacebookPermissionsKey : @[@"publish_actions"] };
    
    [accountStore requestAccessToAccountsWithType:facebookAccountType options:dict completion:^(BOOL granted, NSError *error) {
        if (error) {
            NSLog(@"%@" , error.localizedDescription);
        } else {
            if (!granted) {
               NSLog(@"%@" , error.localizedDescription);
            } else {
                ACAccount *facebookAcccount = (ACAccount *) [[accountStore accountsWithAccountType:facebookAccountType] lastObject];
                if ([facebookAcccount isKindOfClass:[ACAccount class]]) {
                    NSDictionary *feedPublishParams = @{@"access_token" : facebookAcccount.credential.oauthToken,
                                                        @"message" : message };
                    
                    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                            requestMethod:SLRequestMethodPOST
                                                                      URL:[[self class] graphApiFeedEndPoint]
                                                               parameters:feedPublishParams];
                    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                        
                        if (error) {
                            completion(nil, error);
                        } else {
                            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData
                                                                                         options:NSJSONReadingMutableLeaves
                                                                                           error:nil];
                            completion(responseDict, nil);
                        }
                    }];
                }

            }
        }
    }];
}

+ (NSURL *) graphApiFeedEndPoint {
    return [NSURL URLWithString:@"https://graph.facebook.com/me/feed/"];
}

@end
