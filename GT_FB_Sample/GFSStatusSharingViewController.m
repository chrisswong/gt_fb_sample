//
//  GFSStatusSharingViewController.m
//  GT_FB_Sample
//
//  Created by chris on 22/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "GFSStatusSharingViewController.h"
#import "GFSFacebookManager.h"

@interface GFSStatusSharingViewController()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) FacebookClient *facebookClient;

@end

@implementation GFSStatusSharingViewController

#pragma mark - Getter

- (FacebookClient *) facebookClient {
    if (!_facebookClient) {
        _facebookClient = [FacebookClient sharedClient];
    }
    return _facebookClient;
}

#pragma mark - Actions

- (IBAction)postBarButtonDidTap:(UIBarButtonItem *)sender {
    
    if (self.shareType == GFSStatusSharingTypeSystem) {
        [self shareWithSystemLogin];
    } else {
        [self shareWithFacebookSDKLogin];
    }

}

#pragma mark - Private

- (void) shareWithSystemLogin {
    [GFSFacebookManager systemStatusShareWithMessage:self.textView.text completion:^(id result, NSError *error) {
        if (error) {
            NSLog(@"error = %@", error.localizedDescription);
        } else {
            NSLog(@"result = %@" , result);
        }
    }];
}

- (void) shareWithFacebookSDKLogin {
    [GFSFacebookManager statusShareWithMessage:self.textView.text completion:^(FBRequestConnection *connection, id result, NSError *error) {
        if (error) {
            NSLog(@"error = %@", error.localizedDescription);
        } else {
            NSLog(@"result = %@" , result);
        }
    }];
}


@end
