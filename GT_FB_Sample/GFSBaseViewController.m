//
//  GFSBaseViewController.m
//  GT_FB_Sample
//
//  Created by Chris on 24/3/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "GFSBaseViewController.h"

@interface GFSBaseViewController ()

@end

@implementation GFSBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) prefersStatusBarHidden {
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
